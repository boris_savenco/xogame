var React = require('react');
var XOStore = require('../../stores/xostore');
var XOActions = require('../../actions/xoactions.js');

var NewGameBtn = React.createClass({
  handlerClick: function(){
    XOActions.newGame(this.props.gameType);
  },
  render: function(){
    return (
      <div className="btn-group" role="group">
        <button onClick={this.handlerClick} title={this.props.title} className="btn btn-lg btn-warning">{this.props.children}</button>
      </div>
    )
  }
});

module.exports = NewGameBtn;
