var React = require('react');
var XOStore = require('../../stores/xostore');
var NewGameBtn = require('../../components/newgame/newGameBtn');

function needToShow(){
  return {
    needToShow: !XOStore.isGameStarted()
  }
}

var NewGameBoard = React.createClass({
  getInitialState: function(){
    return needToShow();
  },
  componentWillMount: function(){
    XOStore.addChangeListener(this._onNewGame);
  },
  _onNewGame: function(){
    this.setState(needToShow());
  },
  render: function(){
    var htm = null;
    if (this.state.needToShow)
    htm = (
      <div>
        <h2>Начать новую игру:</h2>
        <div className="btn-group btn-group-justified" role="group">
          <NewGameBtn gameType={1} title="Играть за нолик"><i className="fa fa-lg fa-fw fa-times"></i></NewGameBtn>
          <NewGameBtn gameType={-1} title="Играть за крестик"><i className="fa fa-lg fa-fw fa-circle-o"></i></NewGameBtn>
          <NewGameBtn gameType={0}  title="Случайно"><i className="fa fa-lg fa-fw fa-random"></i></NewGameBtn>
        </div>
      </div>
      );
    return htm;
  }
});

module.exports = NewGameBoard;
