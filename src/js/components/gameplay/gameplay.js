var React = require('react');
var XOStore = require('../../stores/xostore');
var GameCell = require('./gameCell');

function getGameBoard(){
  return {
    board: XOStore.getBoard(),
    isGameStarted: XOStore.isGameStarted(),
    cantMove: !XOStore.isCanMove(),
    gameOverMap: XOStore.getGameOverMap()
  };
}

var GamePlay = React.createClass({
  getInitialState: function(){
    return getGameBoard();
  },
  componentWillMount: function(){
    XOStore.addChangeListener(this._onChange);
  },
  _onChange: function(){
    this.setState(getGameBoard());
  },
  render: function(){
    var tbl = null,
        cantMove = this.state.cantMove,
        gameOverMap = this.state.gameOverMap;

    if (this.state.isGameStarted){

      tbl = this.state.board.map(function(value, index){
          var gameOverMove = gameOverMap && ~$.inArray(index, gameOverMap) && true,
              toRet = [<GameCell key={index} index={index} value={value} disabled={cantMove} gameOverMove={gameOverMove} />];
          if (index === 3 || index === 6){
            toRet.unshift(<br />);
          }
          return toRet;
        })
    }
    return (
      <div className="text-center">
        {tbl}
      </div>
    );
  }
});

module.exports = GamePlay;
