var React = require('react');
var XOStore = require('../../stores/xostore');
var XOActions = require('../../actions/xoactions.js');

var gameCell = React.createClass({
  clickHandler: function(){
    if (!this.props.disabled)
      XOActions.makeMove(this.props.index);
  },
  render: function(){
    var btnClassName = 'btn btn-lg',
        spanClass = 'fa fa-fw';
    btnClassName += this.props.gameOverMove ? ' btn-success' : ' btn-primary';

    if (this.props.value){
      btnClassName += ' disabled';
      spanClass += this.props.value === -1 ? ' fa-circle-o' : ' fa-times';
    } else if (this.props.disabled){
      btnClassName += ' disabled';
    }

    return (
      <button className={btnClassName} onClick={this.clickHandler}>
        <span className={spanClass}></span>
      </button>
    )
  }
});

module.exports = gameCell;
