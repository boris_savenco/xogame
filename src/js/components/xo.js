var React = require('react');
var NewGame = require('./newgame/newGame');
var Gameplay = require('./gameplay/gameplay');
var GameOver = require('./gameover/gameover');

var APP =
  React.createClass({
    render: function(){
      return (
        <div className="col-md-6 col-md-offset-3">
          <h1>Крестики-нолики v0.1</h1>
          <NewGame />
          <Gameplay />
          <GameOver />
        </div>
      )
    }
  });

module.exports = APP;
