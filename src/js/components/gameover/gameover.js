var React = require('react');
var XOStore = require('../../stores/xostore');

function getGameOverState(){
  return {
    gameOverType: XOStore.getGameOverType()
  }
}

var GameOver = React.createClass({
  getInitialState: function(){
    return getGameOverState();
  },
  componentWillMount: function(){
    XOStore.addChangeListener(this._onChange);
  },
  _onChange: function(){
    this.setState(getGameOverState());
  },
  _refreshPage: function(){
    window.location.reload();
  },
  render: function(){
    var whoWin = '',
      toRet = null;
    if(this.state.gameOverType !== false){
      switch(this.state.gameOverType){
        case 2:
          whoWin = "компьютер победил";
          break;
        case 1:
          whoWin = "вы победили";
          break;
        case 0:
          whoWin = "ничья";
          break;
      }
      toRet = (
        <div>
          <h2>Игра окончена! <small>{whoWin}</small></h2>
          <button className="btn btn-block btn-lg btn-warning" onClick={this._refreshPage}>Начать еще одну игру</button>
        </div>
      )

    }
    return toRet;
  }
});

module.exports = GameOver;
