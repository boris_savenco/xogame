module.exports = {
  NEW_GAME        : 'NEW_GAME',
  MAKE_MOVE       : 'MAKE_MOVE',
  MOVE_FROM_SERVER: 'MOVE_FROM_SERVER'
}
