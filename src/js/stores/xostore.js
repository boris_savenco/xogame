var Dispatcher = require('../dispatchers/dispatcher');
var XOConstants = require('../constants/xoconstants');
var XOActions = require('../actions/xoactions');
var EventEmitter = require('events').EventEmitter;
console.log(Dispatcher);
var assign = require('object-assign');

var CHANGE_EVENT = 'change',
    EMPTY_PLAYER_TYPE = 0;

var _game = {
  board: [],
  disableMove: false,
  over: false,
  clear: function(){
    this.board = [
      0, 0, 0,
      0, 0, 0,
      0, 0, 0
    ];
    this.disableMove = false;
    this.over = false;
  },
}
_game.clear();

var _player = {
  EMPTY: 0,
  type: 0,
  enemyType: 0,
  changeTo: function(playerType){
    if (!!playerType){
      this.type = playerType;
    } else {
      this.type = Math.random() < 0.5 ? -1 : 1;
    }
    this.enemyType = this.type === -1 ? 1 : -1;
  },
  clear: function(){
    this.type = this.EMPTY;
    this.enemyType = this.EMPTY;
  }
}
_player.clear();

/**
 * Начать новую игру
 * @param  {playerType} playerType установть игроку тип "фишек" {'-1':O, '1':X, '0':случайно}
 * @return {[type]}            [description]
 */
function _newGame(playerType, callback){
  _player.changeTo(playerType);
  _game.clear();
  if (_player.type === -1){
    _disableMove = true;
    var data = {
      player_is_x: false,
      board: _game.board
    }
    $.post('xo_engine.php', data, XOActions.moveFromServer);
  }

}

function _makeMove(cellIndex){
  var data = {
    player_is_x: _player.type === 1
  }
  if(_game.board[cellIndex] === 0){
    _game.board[cellIndex] = _player.type;
    data.board = _game.board;
    _game.disableMove = true;
    $.post('xo_engine.php', data, XOActions.moveFromServer);
  }
}
/**
 * Обработчик хода пришедшего с сервера
 * @return {[type]} [description]
 */
function _moveFromServer(result){

  if (typeof result.move !== 'undefined'){
    _game.board[result.move] = _player.enemyType;
  }
  if (typeof result.game_over !== 'undefined'){
    _game.over = result.game_over;
    _player.clear();
  } else {
    _game.disableMove = false;
  }
}

var AppStore = assign(EventEmitter.prototype, {
  emitChange: function(){
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(callback){
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback){
    this.removeListener(CHANGE_EVENT, callback);
  },
  getPlayerType: function(){
    return _player.type;
  },
  getBoard: function(){
    return _game.board;
  },
  getGameOverMap: function(){
    if(_game.over){
      return _game.over.map;
    }
    return false;
  },
  getGameOverType: function(){
    return typeof _game.over.type !== 'undefined' && _game.over.type;
  },
  isGameStarted: function(){
    return _player.type || !!_game.over;
  },
  isCanMove: function(){
    return _game.disableMove === false && typeof _game.over.map === 'undefined';
  },
  dispatcherIndex: Dispatcher.register(function(payload){
    var action = payload.action;
    switch(payload.actionType){

      case XOConstants.NEW_GAME:
        _newGame(payload.playerType);
        break;

      case XOConstants.MAKE_MOVE:
        _makeMove(payload.index);
        break;

      case XOConstants.MOVE_FROM_SERVER:
        _moveFromServer(payload.responce);
        break;
    }
    AppStore.emitChange();
    return true;
  })
});

module.exports = AppStore;
