var React = require('react');
var ReactDOM = require('react-dom');
var AppXO = require('./components/xo');

ReactDOM.render(
  <AppXO />,
  document.querySelector('#main')
)
