var XOConstants = require('../constants/xoconstants');
var Dispatcher = require('../dispatchers/dispatcher');


var XoActions = {
  newGame: function(playerType){
    Dispatcher.dispatch({
      actionType: XOConstants.NEW_GAME,
      playerType: playerType
    });
  },
  makeMove: function(cellIndex){
    Dispatcher.dispatch({
      actionType: XOConstants.MAKE_MOVE,
      index: cellIndex
    });
  },
  moveFromServer: function(responce){
    Dispatcher.dispatch({
      actionType: XOConstants.MOVE_FROM_SERVER,
      responce: responce
    });
  }
}

module.exports = XoActions;
