<?php
	header('Content-Type: application/json');

	define('XO_EMPTY_CELL', 0);
	define('XO_CELL_X', 1);
	define('XO_CELL_O', -1);
	$variants = [
		[0,1,2],
		[0,3,6],
		[0,4,8],
		[1,4,7],
		[2,5,8],
		[2,4,6],
		[3,4,5],
		[6,7,8]
	];
	$board = $_POST['board'];

	foreach($board as $key=>$val){
		$board[$key] = (int)$val;
	}
	$player_x = json_decode($_POST['player_is_x']);
	//Поехали
	if($player_x){
		$me 		= XO_CELL_O;
		$enemy 	= XO_CELL_X;
	} else {
		$me 		= XO_CELL_X;
		$enemy 	= XO_CELL_O;
	}

	$gameOver = isGameOver($board, $me, $enemy);

	if($gameOver !== false){
		$ansver_arr = [
			'game_over' => $gameOver
		];
	} else {
		$move = getMakeMove($board, $me, $enemy);
		$board[$move] = $me;
		$gameOver = isGameOver($board, $me, $enemy);
		$ansver_arr = [
			'move' 			=> $move
		];
		if($gameOver !== false){
				$ansver_arr['game_over'] = $gameOver;
		}
	}

	echo json_encode($ansver_arr);


	function isGameOver($board, $me, $enemy){
		global $variants;
		//шаг 1. если в 1 из ВВ все клетки одинаковые - это победа
		foreach ($variants as $tRow){
			$summ = $board[$tRow[0]] + $board[$tRow[1]] + $board[$tRow[2]];
			if(	$summ === $enemy * 3 ){
				return [
					'type' => 1,
					'map'  => $tRow
				]; //компьютер проиграл (как?)
			} else if(	$summ === $me * 3 ){
				return [
					'type' => 2,
					'map'  => $tRow
				]; // пользователь проиграл
			}
		}
		if(array_search(XO_EMPTY_CELL, $board) === false) return [
			'type' => 0
		];;
		return false;
	}

	/**
	 * Вернет номер клетки, в которую походит робот
	 * @param  Array $board игральная достка
	 * @return Integer      номер ячейки, в которую будет совершен ход
	 */
	function getMakeMove($board, $me, $enemy){
		global $variants;
		$empty_cell = XO_EMPTY_CELL;

		$cellScore = [];
		for ($i = 0; $i < 9; $i++){
			//Если сюда можно ходить
			if ($board[$i] === 0){
				if($i === 4){
					$tScore = 2;
				} else {
					//Приоритет - угол
					$tScore = $i % 2 === 0 ? 1 : 0;
				}
				foreach ($variants as $tRow){
					//Если это число встречается в одной из выиграшных комбинаций
					if($tRow[0] === $i || $tRow[1] === $i || $tRow[2] === $i) {

						if($tRow[0] === $i) {
							$thisCell = $board[$tRow[0]];
							$notThisCell1 = $board[$tRow[1]];
							$notThisCell2 = $board[$tRow[2]];
						} else if ($tRow[1] === $i){
							$thisCell = $board[$tRow[1]];
							$notThisCell1 = $board[$tRow[0]];
							$notThisCell2 = $board[$tRow[2]];
						} else if ($tRow[2] === $i){
							$thisCell = $board[$tRow[2]];
							$notThisCell1 = $board[$tRow[0]];
							$notThisCell2 = $board[$tRow[1]];
						}

						//Наступление
						if(
							//если ни одна из клеток этого ряда не занята другим игроком
							$notThisCell1 !== $enemy && $notThisCell2 !== $enemy
						){
							$tScore += 1;

							if ($notThisCell1 === $me && $notThisCell2 === $me){
								$tScore += 100;
							}

						//Если эта клатка пуста, а 2 остальные заняты игроком - они имеют наивышсший приоритет
						} else if ($notThisCell1 === $enemy && $notThisCell2 === $enemy){
								$tScore += 50;
						} //оборона
					} // число не встречается в этом ряду
				}// перебор выиграшных рядов
			} else { // если эта клетка не пуста
				$tScore = -1;
			}
			$cellScore[] = $tScore;
		}
		asort($cellScore);
		$chance = end($cellScore);
		return key($cellScore);
	}
